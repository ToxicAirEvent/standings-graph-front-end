import React from 'react';
import axios from 'axios';
import {Line} from 'react-chartjs-2';
import { defaults } from 'react-chartjs-2';

import './App.css';

import * as colors from './helpers/teamColors.js';
import * as utils from './helpers/utils.js';
import * as secondaryGraphs from './helpers/secondaryGraphs.js';

/*
	Chartjs in their exaples has constants defined for the containers of all graphs.
	So these layout the baselines for each one that will be contained on this page.
	This is likely because the chart is first inserted into the page as an empty object.
	Then once the component mounts the data can be populated dynamically.
*/

defaults.global.defaultFontFamily = 'effra, sans-serif';

const allTeams = {
  labels: [],
  datasets: [],
  datasetKeyProvider:'EverySingleTeam'
}

const nlEastStandings = {
	labels:[],
	datasets:[],
	datasetKeyProvider:'TheNationalLeagueEast'
}

const nlCentralStandings = {
	labels:[],
	datasets:[],
	datasetKeyProvider:'TheNationalLeagueCentral'
}

const nlWestStandings = {
	labels:[],
	datasets:[],
	datasetKeyProvider:'TheNationalLeagueWest'
}

const alEastStandings = {
	labels:[],
	datasets:[],
	datasetKeyProvider:'TheAmericanLeagueEast'
}

const alCentralStandings = {
	labels:[],
	datasets:[],
	datasetKeyProvider:'TheAmericanLeagueCentral'
}

const alWestStandings = {
	labels:[],
	datasets:[],
	datasetKeyProvider:'TheAmericanLeagueWest'
}

export default class CompleteGraph extends React.Component{
	constructor(props){
		super(props);
	}

	//Because of the API request through axios to populate many sets of data component mounting works best when handled as async.
	async componentDidMount(){
		var teamColorsList = colors.teamColorsList();
		var readableDates = [];

		//Make the API request to the backend to get the standings data by day.
		var standingsAPIRequest = await axios.get('https://api.the-standings.com/standings');

		//Pass off the varios data points within the response request to make things cleaner to handle.
		const teams = standingsAPIRequest.data.teamStandings;
		const standingsDates = standingsAPIRequest.data.dates;	
		
		/*
			Erik Berg's API (erikberg.com) where this data comes from handles date lookups in the yyyymmdd format.
			So in the local database this format is retained. However it isn't always the nicest human readable form.
			So a function was written to format the dates to something human readable, push them into the main standings graph labels, 
			and then also store them within an array for use elsewhere on other charts.
		*/
		for(const date of standingsDates){
		let readableDate = utils.formatDate(date);
			allTeams.labels.push(readableDate);
			readableDates.push(readableDate); //These dates will be kept on their own for use in divisional data sets.
		}

		//Push the data for each team into the dataset of the masterchart. This should like become a utility function later on.
		for(const graphTeam of teams){
			let teamGraphTemplate = {
				label: graphTeam.last_name,
				backgroundColor: 'transparent',
				borderColor: teamColorsList[graphTeam.team_id].primary,
				borderWidth: 2,
				hoverBackgroundColor: teamColorsList[graphTeam.team_id].secondary,
				pointBackgroundColor: teamColorsList[graphTeam.team_id].primary,
				hoverBorderColor: 'rgba(255,99,132,1)',
				data: graphTeam.winsByDay
			}

			allTeams.datasets.push(teamGraphTemplate);
		}

		//Request datasets be made for each division in major league baseball as part of our secondary graphs builder utility.
		var alEast = await secondaryGraphs.graphDivision('AL','E',teams,readableDates);
		var alCentral = await secondaryGraphs.graphDivision('AL','C',teams,readableDates);
		var alWest = await secondaryGraphs.graphDivision('AL','W',teams,readableDates);

		var nlEast = await secondaryGraphs.graphDivision('NL','E',teams,readableDates);
		var nlCentral = await secondaryGraphs.graphDivision('NL','C',teams,readableDates);
		var nlWest = await secondaryGraphs.graphDivision('NL','W',teams,readableDates);

		//Pass all of those standings datapoints into the variables that make up each graphs datasets.
		alEastStandings['labels'] = alEast.labels;
		alEastStandings['datasets'] = alEast.datasets;

		alCentralStandings['labels'] = alCentral.labels;
		alCentralStandings['datasets'] = alCentral.datasets;

		alWestStandings['labels'] = alWest.labels;
		alWestStandings['datasets'] = alWest.datasets;

		nlEastStandings['labels'] = nlEast.labels;
		nlEastStandings['datasets'] = nlEast.datasets;

		nlCentralStandings['labels'] = nlCentral.labels;
		nlCentralStandings['datasets'] = nlCentral.datasets;

		nlWestStandings['labels'] = nlWest.labels;
		nlWestStandings['datasets'] = nlWest.datasets;

		this.setState({teams});
	}

	render(){
		return(
			<div className="App">
		      <h1 className="text-center">The Entire League Over Time</h1>
		      <p className="text-center sub-text">A visual representation of each teams wins by day (or most days) since the start of the shortened 2020 MLB season.</p>

		      <Line
		        data={allTeams}
		        width={50}
		        height={20}
		        options={{maintainAspectRatio: true}}
		      />

		      <h2 className="text-center league-divider">-THE AMERICAN LEAGUE-</h2>
 
		      <h3 className="text-center division-divider">The AL East</h3>
		      <Line
		        data={alEastStandings}
		        width={50}
		        height={20}
		        options={{ maintainAspectRatio: true }}
		      />

		      <h3 className="text-center division-divider">The AL Central</h3>
		      <Line
		        data={alCentralStandings}
		        width={50}
		        height={20}
		        options={{ maintainAspectRatio: true }}
		      />

		      <h3 className="text-center division-divider">The AL East</h3>
		      <Line
		        data={alWestStandings}
		        width={50}
		        height={20}
		        options={{ maintainAspectRatio: true }}
		      />

		      <h2 className="text-center league-divider">-THE NATIONAL LEAGUE-</h2>
 
		      <h3 className="text-center division-divider">The NL East</h3>
		      <Line
		        data={nlEastStandings}
		        width={50}
		        height={20}
		        options={{ maintainAspectRatio: true }}
		      />

		      <h3 className="text-center division-divider">The NL Central</h3>
		      <Line
		        data={nlCentralStandings}
		        width={50}
		        height={20}
		        options={{ maintainAspectRatio: true }}
		      />

		      <h3 className="text-center division-divider">The NL West</h3>
		      <Line
		        data={nlWestStandings}
		        width={50}
		        height={20}
		        options={{ maintainAspectRatio: true }}
		      />
		    </div>
		)
	}
}