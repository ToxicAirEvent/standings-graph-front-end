import React, {useEffect, createRef} from 'react';
import lottie from 'lottie-web';
import animationData from './animations/who-is-header-data.json';

import './App.css';

function AnimatedHeader(){

	let animationContainer = createRef();

	React.useEffect(() => {
		const anim = lottie.loadAnimation({
			container: animationContainer.current,
			renderer: "svg",
			loop: true,
			autoplay: true,
			animationData: animationData
		});

		return () => anim.destroy(); // optional clean up for unmounting
	}, []);

	return (
		<div className="App">
			<div className="animation-container main-header-stadium" ref={animationContainer} />
		</div>
	)
}

export default AnimatedHeader;