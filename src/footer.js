import React from 'react';

import './App.css';

export default class SiteFooter extends React.Component{
	render(){
		return(
			<div className="App">
		      <p className="text-center league-divider">&copy; 2020 - <a href="https://jackmceachern.com">Jack McEachern</a></p>
		    </div>
		)
	}
}