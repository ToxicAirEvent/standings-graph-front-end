import * as colors from './teamColors.js';

const teamColors = colors.teamColorsList();

export async function graphDivision(league,division,standingsData,dates){
	var divisionData = {
		labels: dates,
		datasets: []
	}

	for(var singleTeam of standingsData){
		if(singleTeam.conference === league && singleTeam.division === division){
			let divisionTeam = {
				label: singleTeam.last_name,
				backgroundColor: 'transparent',
				borderColor: teamColors[singleTeam.team_id].primary,
				borderWidth: 2,
				hoverBackgroundColor: teamColors[singleTeam.team_id].secondary,
				hoverBorderColor: 'rgba(255,99,132,1)',
				data: singleTeam.winsByDay
			}

			divisionData.datasets.push(divisionTeam);
		}
	}

	return divisionData;
}