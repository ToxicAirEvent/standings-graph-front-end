export function formatDate(singleDate){
	let year = singleDate.substring(0,4);
	let month = singleDate.substring(4,6);
	let day = singleDate.substring(6,8);

	let standingDate = new Date(year,month-1,day);

	let namedMonth = monthsArray[standingDate.getMonth()];

	let easyReadable = namedMonth + " " + standingDate.getDate();

	return easyReadable;
}

export function isInView(watchedElement){}

var monthsArray = [
	"January",
	"February",
	"March",
	"April",
	"May",
	"June",
	"July",
	"August",
	"September",
	"October",
	"November",
	"December"
];