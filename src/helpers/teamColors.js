export function teamColorsList(){
	var teamColors = {
		'cleveland-indians': {
			primary: '#0C2340',
			secondary: '#E31937'
		},
		'minnesota-twins': {
			primary: '#002B5C',
			secondary: '#D31145'
		},
		'kansas-city-royals': {
			primary: '#004687',
			secondary: '#BD9B60'
		},
		'detroit-tigers': {
			primary: '#0C2340',
			secondary: '#FA4616'
		},
		'chicago-white-sox': {
			primary: '#27251F',
			secondary: '#C4CED4'
		},
		'tampa-bay-rays': {
			primary: '#8FBCE6',
			secondary: '#F5D130'
		},
		'new-york-yankees': {
			primary: '#003087',
			secondary: '#E4002C'
		},
		'baltimore-orioles': {
			primary: '#DF4601',
			secondary: '#000000'
		},
		'toronto-blue-jays': {
			primary: '#134A8E',
			secondary: '#E8291C'
		},
		'boston-red-sox': {
			primary: '#BD3039',
			secondary: '#0C2340'
		},
		'oakland-athletics': {
			primary: '#003831',
			secondary: '#EFB21E'
		},
		'houston-astros': {
			primary: '#002D62',
			secondary: '#EB6E1F'
		},
		'texas-rangers': {
			primary: '#003278',
			secondary: '#C0111F'
		},
		'seattle-mariners': {
			primary: '#005C5C',
			secondary: '#0C2C56'
		},
		'los-angeles-angels': {
			primary: '#003263',
			secondary: '#BA0021'
		},
		'chicago-cubs': {
			primary: '#0E3386',
			secondary: '#CC3433'
		},
		'st-louis-cardinals': {
			primary: '#C41E3A',
			secondary: '#0C2340'
		},
		'milwaukee-brewers': {
			primary: '#FFC52F',
			secondary: '#12284B'
		},
		'cincinnati-reds': {
			primary: '#C6011F',
			secondary: '#000000'
		},
		'pittsburgh-pirates': {
			primary: '#FDB827',
			secondary: '#27251F'
		},
		'miami-marlins': {
			primary: '#00A3E0',
			secondary: '#EF3340'
		},
		'new-york-mets': {
			primary: '#002D72',
			secondary: '#FF5910'
		},
		'atlanta-braves': {
			primary: '#CE1141',
			secondary: '#13274F'
		},
		'philadelphia-phillies': {
			primary: '#E81828',
			secondary: '#002D72'
		},
		'washington-nationals': {
			primary: '#AB0003',
			secondary: '#14225A'
		},
		'san-diego-padres': {
			primary: '#2F241D',
			secondary: '#FFC425'
		},
		'colorado-rockies': {
			primary: '#33006F',
			secondary: '#C4CED4'
		},
		'san-francisco-giants': {
			primary: '#FD5A1E',
			secondary: '#27251F'
		},
		'los-angeles-dodgers': {
			primary: '#005A9C',
			secondary: '#EF3E42'
		},
		'arizona-diamondbacks': {
			primary: '#A71930',
			secondary: '#E3D4AD'
		}

	}

	return teamColors;
}
